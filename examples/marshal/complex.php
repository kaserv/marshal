<?php

use Sk\Marshal\Annotations\XmlAttributes;
use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\Annotations\XmlElement;
use Sk\Marshal\Annotations\XmlElementsWrapper;
use Sk\Marshal\Serializer;

require_once '../../vendor/autoload.php';


/**
 * @XmlDocument(name="RegisteredUser",
 *              namespace="users",
 *              attributes={"group" = "registered", "role" = "default"},
 *              order={"profile", "body", "login"})
 */
class User
{
    /**
     * @XmlElement(attributes={"type": "email"})
     * @var string
     */
    public $login;
    /**
     * @var array
     * @XmlElementsWrapper
     * @XmlElement
     */
    public $body = [];
    /**
     * @XmlAttributes
     * @var string[]
     */
    private $info = [];
    /**
     * @var Profile
     * @XmlElement(name="Профиль")
     */
    private $profile;

    public function __construct($email, $role)
    {
        $this->login = $email;
        $this->info = [
            'registeredAt' => date('Y-m-d'),
            'role' => $role
        ];
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     */
    public function setProfile(Profile $profile)
    {
        $this->profile = $profile;
    }
}

/**
 * @XmlElement
 */
class Profile
{
    /**
     * @XmlAttributes()
     */
    public $attributes = [
        'attr' => 'val'
    ];

    /**
     * @XmlElement
     */
    public $firstName;

    /**
     * @XmlElement
     */
    public $lastName;

    /**
     * @XmlElement
     */
    public $birthDate;

    /**
     * @XmlElement(name="ThirdLevel")
     */
    public $addition;
}

/**
 * @XmlElement
 */
class ArrayField
{
    /**
     * @XmlElement()
     */
    public $value;

    public function __construct()
    {
        $this->value = [
            rand(1, 10),
            rand(1, 10),
            rand(1, 10)
        ];
    }
}

/**
 * @XmlElement
 */
class MyClass
{
    /**
     * @XmlElement()
     */
    public $asd;

    /**
     * @XmlElement()
     */
    public $qwe;

    public function __construct()
    {
        $this->asd = rand(1, 100);
        $this->qwe = rand(1, 100);
    }
}

$profile = new Profile();
$profile->firstName = 'John';
$profile->lastName = 'Doe';
/** @noinspection PhpUnhandledExceptionInspection */
$profile->birthDate = (new DateTime('01 jan 1989'))->format('d.m.Y');
$profile->addition = [
    new ArrayField(),
    new ArrayField(),
    new ArrayField()
];
$user = new User('user@example.com', 'manager');
$user->setProfile($profile);
$user->body[] = new MyClass();
$user->body[] = new MyClass();

$converter = new Serializer();
/** @noinspection PhpUnhandledExceptionInspection */
$doc = $converter->serialize($user);
$doc->formatOutput = true;
echo $doc->saveXML();

/*
Output:

<?xml version="1.0" encoding="utf-8"?>
<users:RegisteredUser group="registered" role="manager" registeredAt="2018-11-23">
  <body group="registered" role="manager" registeredAt="2018-11-23">
    <MyClass>
      <asd>44</asd>
      <qwe>96</qwe>
    </MyClass>
    <MyClass>
      <asd>86</asd>
      <qwe>89</qwe>
    </MyClass>
  </body>
  <login>user@example.com</login>
  <Profile attr="val">
    <firstName>John</firstName>
    <lastName>Doe</lastName>
    <birthDate>01.01.1989</birthDate>
    <ThirdLevel>
      <value>2</value>
      <value>8</value>
      <value>2</value>
    </ThirdLevel>
    <ThirdLevel>
      <value>3</value>
      <value>6</value>
      <value>1</value>
    </ThirdLevel>
    <ThirdLevel>
      <value>2</value>
      <value>10</value>
      <value>10</value>
    </ThirdLevel>
  </Profile>
</users:RegisteredUser>
*/