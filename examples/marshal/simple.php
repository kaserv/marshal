<?php


use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\Annotations\XmlElement;
use Sk\Marshal\Serializer;

require_once '../../vendor/autoload.php';

/**
 * Class SimpleExample
 *
 * @XmlDocument
 */
class SimpleExample
{
    /**
     * @var string
     * @XmlElement(name="CustomFoo")
     */
    private $foo;

    public function __construct(string $foo)
    {
        $this->foo = $foo;
    }

    /**
     * @return string
     */
    public function getFoo(): string
    {
        return $this->foo;
    }
}

$simpleExample = new SimpleExample('bar');
$converter = new Serializer();
/** @noinspection PhpUnhandledExceptionInspection */
$doc = $converter->serialize($simpleExample);
$doc->formatOutput = true;
echo $doc->saveXML();

/*
Output:

<?xml version="1.0" encoding="utf-8"?>
<SimpleExample>
  <CustomFoo>bar</CustomFoo>
</SimpleExample>
*/