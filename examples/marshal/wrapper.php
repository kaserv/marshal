<?php

use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\Annotations\XmlElement;
use Sk\Marshal\Annotations\XmlElementsWrapper;
use Sk\Marshal\Annotations\XmlElementEnum;
use Sk\Marshal\Serializer;

require_once '../../vendor/autoload.php';

/**
 * @XmlDocument()
 */
class Document
{
    /**
     * @XmlElement()
     */
    public $foo = 'bar';

    /**
     * @XmlElementsWrapper()
     */
    public $body = [];

    /**
     * @XmlElement()
     */
    public $customArr = [];

    /**
     * @XmlElementEnum(map={"FirstNode": "enumFirst", "SecondNode": "enumSecond", "SimpleNode": "simpleNodeElem"})
     */
    public $enum = null;
}

/**
 * @XmlElement(name="Первый")
 */
class FirstNode
{
    /**
     * @XmlElement()
     */
    public $first = 'firstValue';

    /**
     * @XmlElement()
     */
    public $second = 'secondValue';
}

/**
 * @XmlElement()
 */
class SecondNode
{
    /**
     * @XmlElement()
     */
    public $third = 'thirdValue';

    /**
     * @XmlElement()
     */
    public $fourth = 'fourthValue';
}

/**
 * @\Sk\Marshal\Annotations\XmlElementString()
 */
class SimpleNode
{
    public $value = 'simpleNodeValue';
}

$document = new Document();
$document->body[] = new FirstNode();
$document->body[] = new FirstNode();
$document->body[] = new FirstNode();
$document->body[] = new SecondNode();
$document->body[] = new SecondNode();
$document->customArr[] = new FirstNode();
$document->customArr[] = new FirstNode();
$document->enum = new SimpleNode();

$converter = new Serializer();
/** @noinspection PhpUnhandledExceptionInspection */
$doc = $converter->serialize($document);
$doc->formatOutput = true;
echo $doc->saveXML();

/*
Output:

<?xml version="1.0" encoding="utf-8"?>
<Document>
  <foo>bar</foo>
  <body>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <SecondNode>
      <third>thirdValue</third>
      <fourth>fourthValue</fourth>
    </SecondNode>
    <SecondNode>
      <third>thirdValue</third>
      <fourth>fourthValue</fourth>
    </SecondNode>
  </body>
  <customArr>
    <first>firstValue</first>
    <second>secondValue</second>
  </customArr>
  <customArr>
    <first>firstValue</first>
    <second>secondValue</second>
  </customArr>
  <simpleNodeElem>simpleNodeValue</simpleNodeElem>
</Document>
 */