<?php

use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\Annotations\XmlElement;

require_once '../../vendor/autoload.php';

/**
 * Class SimpleExample
 *
 * @XmlDocument
 */
class SimpleExample
{
    /**
     * @var string
     * @XmlElement(name="CustomFoo")
     */
    private $foo;

    public function __construct(string $foo)
    {
        $this->foo = $foo;
    }

    /**
     * @param $foo
     */
    public function setFoo($foo)
    {
        $this->foo = $foo;
    }
}

$xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<SimpleExample>
  <CustomFoo>bar</CustomFoo>
</SimpleExample>
XML;

$serializer = new Sk\Marshal\Serializer();
/** @noinspection PhpUnhandledExceptionInspection */
$object = $serializer->deserialize($xml, (new Sk\Marshal\ClassMap())
    ->register('SimpleExample', SimpleExample::class)
);
var_dump($object);