<?php

use Sk\Marshal\ClassMap;
use Sk\Marshal\Serializer;

require_once '../../vendor/autoload.php';

/**
 * @Sk\Marshal\Annotations\XmlDocument(skipUnknown=true)
 */
class Document
{
    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $foo = 'bar';

    /**
     * @Sk\Marshal\Annotations\XmlElementsWrapper(skipUnknown=true)
     */
    public $body = [];

    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $customArr = [];

//    /**
//     * @XmlElementEnum(map={"FirstNode": "enumFirst", "SecondNode": "enumSecond", "SimpleNode": "simpleNodeElem"})
//     */
//    public $enum = null;
}

/**
 * @Sk\Marshal\Annotations\XmlElement(name="Первый")
 */
class FirstNode
{
    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $first = 'firstValue';

    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $second = 'secondValue';
}

/**
 * @Sk\Marshal\Annotations\XmlElement()
 */
class SecondNode
{
    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $third = 'thirdValue';

    /**
     * @Sk\Marshal\Annotations\XmlElement()
     */
    public $fourth = 'fourthValue';
}

/**
 * @\Sk\Marshal\Annotations\XmlElementString()
 */
class SimpleNode
{
    public $value = 'simpleNodeValue';
}


$xml = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<Document>
  <foo>bar</foo>
  <Unknown>
    <unknownChild>adf</unknownChild>
  </Unknown>
  <body>
    <Unknown>
      <unknownChild>adf</unknownChild>
    </Unknown>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <Первый>
      <first>firstValue</first>
      <second>secondValue</second>
    </Первый>
    <SecondNode>
      <third>thirdValue</third>
      <fourth>fourthValue</fourth>
    </SecondNode>
    <SecondNode>
      <third>thirdValue</third>
      <fourth>fourthValue</fourth>
    </SecondNode>
  </body>
  <customArr>
    <first>firstValue</first>
    <second>secondValue</second>
  </customArr>
  <customArr>
    <first>firstValue</first>
    <second>secondValue</second>
  </customArr>
  <simpleNodeElem>simpleNodeValue</simpleNodeElem>
</Document>
XML;

$serializer = new Serializer();
/** @noinspection PhpUnhandledExceptionInspection */
$obj = $serializer->deserialize($xml, (new ClassMap())
    ->register('Document', Document::class)
    ->register('Первый', FirstNode::class)
    ->register('customArr', FirstNode::class)
    ->register('SecondNode', SecondNode::class)
    ->register('FirstNode', FirstNode::class)
//    ->register('SimpleNode', SimpleNode::class)
);
var_dump($obj);