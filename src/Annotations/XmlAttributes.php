<?php

namespace Sk\Marshal\Annotations;

/**
 * XML element attributes annotation.
 * Value must be array like ['attr_name' => 'attr_value']
 *
 * @Annotation
 */
final class XmlAttributes
{
}