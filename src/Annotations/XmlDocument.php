<?php

namespace Sk\Marshal\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * XML document annotation.
 *
 * @Annotation
 */
final class XmlDocument extends XmlElementAnnotation
{
    /**
     * XML Version
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * XML Encoding.
     *
     * @var string
     */
    public $encoding = 'utf-8';
}