<?php

namespace Sk\Marshal\Annotations;

/**
 * XML element annotation
 *
 * @Annotation
 */
final class XmlElement extends XmlElementAnnotation
{
    /**
     * Element type. Required for unmarshalling
     *
     * @var string
     */
    public $type = null;
}