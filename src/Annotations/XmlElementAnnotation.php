<?php

namespace Sk\Marshal\Annotations;

abstract class XmlElementAnnotation
{
    /**
     * Node name.
     *
     * @var string
     */
    public $name = null;

    /**
     * Node namespace.
     *
     * @var string
     */
    public $namespace = null;

    /**
     * Node static attributes.
     *
     * @var array
     */
    public $attributes = [];

    /**
     * Nodes order.
     * All unlisted elements will be unordered.
     *
     * @var array
     */
    public $order = [];

    /**
     * @var bool
     */
    public $skipUnknown = false;
}