<?php

namespace Sk\Marshal\Annotations;

/**
 * XML element annotation
 *
 * @Annotation
 */
final class XmlElementEnum extends XmlElementAnnotation
{
    /**
     * Property name with types map. Required.
     *
     * @var array
     */
    public $map = [];
}