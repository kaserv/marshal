<?php

namespace Sk\Marshal\Annotations;

/**
 * XML element annotation
 *
 * @Annotation
 */
final class XmlElementString extends XmlElementAnnotation
{
}