<?php

namespace Sk\Marshal;

use Doctrine\Common\Annotations\AnnotationRegistry;

/**
 * AnnotationsRegistrar
 *
 * @deprecated Must be removed with doctrine/annotations:2.0 release.
 */
class AnnotationsRegistrar
{
    /** @var bool */
    private static $registered = false;

    public static function register()
    {
        /** @noinspection PhpDeprecationInspection */
        if (!self::$registered) {
            /** @noinspection PhpDeprecationInspection */
            AnnotationRegistry::registerLoader('class_exists');
            /** @noinspection PhpDeprecationInspection */
            self::$registered = true;
        }
    }
}