<?php

namespace Sk\Marshal;


use Sk\Marshal\Exceptions\TagIsNotRegisteredException;

class ClassMap
{
    private $classes = [];

    /**
     * Associate XML tag to class.
     *
     * @param $xmlTag
     * @param $class
     *
     * @return ClassMap
     */
    public function register($xmlTag, $class)
    {
        $this->classes[$xmlTag] = $class;
        return $this;
    }

    /**
     * Return name of class associated with tag
     *
     * @param $tag
     *
     * @return mixed
     * @throws TagIsNotRegisteredException
     */
    public function getClass($tag)
    {
        if (!isset($this->classes[$tag])) {
            throw new TagIsNotRegisteredException($tag);
        }

        return $this->classes[$tag];
    }
}