<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class NotAnnotatedClass extends Exception
{
    /**
     * NoDocumentAttributeException constructor.
     *
     * @param string $className
     * @param string $annotationClass
     */
    public function __construct(string $className, string $annotationClass)
    {
        parent::__construct($className . ' does not have ' . $annotationClass . ' annotation');
    }
}