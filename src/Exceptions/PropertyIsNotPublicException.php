<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class PropertyIsNotPublicException extends Exception
{
    /**
     * PropertyIsNotPublicException constructor.
     *
     * @param $propertyName
     */
    public function __construct($propertyName)
    {
        $message = "Property $propertyName must be public or has a setter";

        parent::__construct($message);
    }
}