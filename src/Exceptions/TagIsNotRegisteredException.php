<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class TagIsNotRegisteredException extends Exception
{
    public function __construct($tag)
    {
        $message = "Tag $tag is not registered.";

        parent::__construct($message);
    }
}