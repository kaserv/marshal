<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class TagNotMatchException extends Exception
{
    public function __construct($actualTag, $expectedTag)
    {
        $message = $actualTag . ' is mot match expected tag: ' . $expectedTag;

        parent::__construct($message);
    }
}