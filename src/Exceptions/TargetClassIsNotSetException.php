<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class TargetClassIsNotSetException extends Exception
{
    public function __construct($class, $property)
    {
        $message = "Property '$property' of class '$class' doesn't have targetClass in annotation.";

        parent::__construct($message);
    }
}