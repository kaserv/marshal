<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class TargetNotMatchException extends Exception
{
    /**
     * TargetNotMatchException constructor.
     *
     * @param object $object
     * @param string $target
     */
    public function __construct($object, string $target)
    {
        $message = 'Object is not instance of ' . $target . ' (' . get_class($object) . ' is given).';

        parent::__construct($message);
    }
}