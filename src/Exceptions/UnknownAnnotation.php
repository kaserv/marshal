<?php

namespace Sk\Marshal\Exceptions;

use Exception;

class UnknownAnnotation extends Exception
{
    private $annotation;

    /**
     * @return mixed
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * @param mixed $annotation
     */
    public function setAnnotation($annotation): void
    {
        $this->annotation = $annotation;
    }
}