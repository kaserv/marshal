<?php

namespace Sk\Marshal;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use ReflectionException;
use Sk\Marshal\Serializer\Marshaller;
use Sk\Marshal\Serializer\Unmarshaller;

class Serializer
{
    public function __construct()
    {
        /** @noinspection PhpDeprecationInspection */
        AnnotationsRegistrar::register();
    }

    /**
     * Create DOMDocument from object annotated with "@XmlDocument"
     *
     * @param object $object
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws Exceptions\NotAnnotatedClass
     * @throws Exceptions\TargetNotMatchException
     * @throws ReflectionException
     * @throws Exceptions\StringElementDoesntHaveValue
     */
    public function serialize($object)
    {
        $marshaller = new Marshaller($object);
        $doc = $marshaller->getDocument();

        return $doc;
    }

    /**
     * Create object of $class from $xml.
     *
     * @param string   $xml              Input xml
     * @param ClassMap $classMap
     * @param string   $staticAttributes Property name for attributes from XmlElement
     * @param string   $unknownProperty  Property name of result object
     *                                   for storing values of unknown tags
     *
     * @return object
     * @throws AnnotationException
     * @throws Exceptions\PropertyIsNotPublicException
     * @throws Exceptions\TagIsNotRegisteredException
     * @throws Exceptions\TagNotMatchException
     * @throws Exceptions\TargetClassIsNotSetException
     * @throws ReflectionException
     * @throws Exceptions\StringElementDoesntHaveValue
     */
    public function deserialize(string $xml, ClassMap $classMap, string $staticAttributes = '__staticAttributes', string $unknownProperty = '__unknownProperties')
    {
        $unmarshaller = new Unmarshaller($xml, $classMap, $staticAttributes, $unknownProperty);
        return $unmarshaller->getObject();
    }
}