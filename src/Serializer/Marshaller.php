<?php

namespace Sk\Marshal\Serializer;

use ArrayAccess;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use DOMDocument;
use DOMElement;
use ReflectionClass;
use ReflectionException;
use Sk\Marshal\Annotations\XmlAttributes;
use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\Annotations\XmlElementAnnotation;
use Sk\Marshal\Annotations\XmlElementString;
use Sk\Marshal\Annotations\XmlElementsWrapper;
use Sk\Marshal\Exceptions\NotAnnotatedClass;
use Sk\Marshal\Exceptions\StringElementDoesntHaveValue;
use Sk\Marshal\Exceptions\TargetNotMatchException;

class Marshaller
{
    /** @var AnnotationReader */
    private $reader;

    /** @var DOMDocument */
    private $document;

    /**
     * Marshaller constructor.
     *
     * @param object $object
     *
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws NotAnnotatedClass
     * @throws TargetNotMatchException
     * @throws StringElementDoesntHaveValue
     */
    public function __construct($object)
    {
        $this->reader = new AnnotationReader();

        $reflection = new ReflectionClass(get_class($object));
        $documentAnnotation = $this->getDocumentAnnotation($reflection);

        $this->document = new DOMDocument($documentAnnotation->version, $documentAnnotation->encoding);

        $this->appendObject($object, $this->document);
    }

    /**
     * Annotation of given object's class.
     *
     * @param ReflectionClass $reflection
     *
     * @return XmlDocument
     * @throws NotAnnotatedClass
     */
    private function getDocumentAnnotation(ReflectionClass $reflection): XmlDocument
    {
        /** @var XmlDocument */
        $documentAnnotation = $this->reader->getClassAnnotation($reflection, XmlDocument::class);

        if (!$documentAnnotation) {
            throw new NotAnnotatedClass($reflection->getName(), XmlDocument::class);
        }

        return $documentAnnotation;
    }

    /**
     * Append object to document.
     *
     * @param object                 $object
     * @param DOMElement|DOMDocument $parent
     * @param string|null            $customName
     *
     * @return void
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws StringElementDoesntHaveValue
     * @throws TargetNotMatchException
     */
    private function appendObject($object, $parent, $customName = null)
    {
        $reflection = new ReflectionClass(get_class($object));
        /** @var XmlElementAnnotation $annotation */
        $annotation = $this->reader->getClassAnnotation($reflection, XmlElementAnnotation::class);

        $name = $customName ?: $this->getFullName($reflection, $annotation);
        $order = $this->getPropsOrder($reflection, $annotation);

        $node = $this->document->createElement($name);
        $node = $this->appendStaticAttributes($node, $annotation);
        $node = $this->appendAttributes($node, $reflection, $object);

        foreach ($order as $field) {
            $propertyReflection = $reflection->getProperty($field);
            /** @var XmlElementAnnotation $propertyAnnotation */
            $propertyAnnotation = $this->reader->getPropertyAnnotation($propertyReflection, XmlElementAnnotation::class);
            $property = new Property($propertyReflection, $object, $propertyAnnotation);

            $nodeName = $property->getName();
            $nodeValue = $property->getValue();

            if (is_array($nodeValue) || $nodeValue instanceof ArrayAccess) {
                if ($propertyAnnotation instanceof XmlElementsWrapper) {
                    $wrapper = $this->document->createElement($nodeName);
                    $wrapper = $this->appendStaticAttributes($wrapper, $propertyAnnotation);
                    $wrapper = $this->appendAttributes($wrapper, $reflection, $object);

                    foreach ($nodeValue as $item) {
                        $this->appendObject($item, $wrapper);
                    }

                    $node->appendChild($wrapper);
                } else {
                    $this->appendArray($nodeName, $nodeValue, $node);
                }
            } elseif (is_object($nodeValue)) {
                $valueReflection = new ReflectionClass($nodeValue);
                $annotationReader = new AnnotationReader();
                $annotation = $annotationReader->getClassAnnotation($valueReflection, XmlElementString::class);
                if (!$annotation) {
                    $this->appendObject($nodeValue, $node, $nodeName);
                } else {
                    if (!isset($nodeValue->value)) {
                        throw new StringElementDoesntHaveValue('Class with XmlElementString must have public property $value');
                    }
                    $this->appendString($nodeName, $nodeValue->value, $node);
                }
            } else {
                $this->appendString($nodeName, $nodeValue, $node);
            }
        }

        $parent->appendChild($node);
    }

    /**
     * Full tag name with namespace.
     *
     * @param ReflectionCLass      $reflection
     * @param XmlElementAnnotation $annotation
     *
     * @return string
     */
    private function getFullName(ReflectionClass $reflection, XmlElementAnnotation $annotation)
    {
        $name = $annotation->name ?? $reflection->getShortName();
        $nameSpace = $annotation->namespace ?? null;
        if (!empty($nameSpace)) {
            $name = implode(':', [$nameSpace, $name]);
        }

        return $name;
    }

    /**
     * Props order for XML object.
     *
     * @param ReflectionClass      $reflection
     * @param XmlElementAnnotation $annotation
     *
     * @return array
     */
    private function getPropsOrder(ReflectionClass $reflection, XmlElementAnnotation $annotation)
    {
        $order = $annotation->order;
        $classProperties = $reflection->getProperties();
        foreach ($classProperties as $property) {
            $annotation = $this->reader->getPropertyAnnotation($property, XmlElementAnnotation::class);
            if (!$annotation) {
                continue;
            }

            if (in_array($property->getName(), $order)) {
                continue;
            }

            $order[] = $property->getName();
        }

        return $order;
    }

    /**
     * Appends attributes from annotation to given node.
     *
     * @param DOMElement           $node
     * @param XmlElementAnnotation $annotation
     *
     * @return DOMElement
     */
    private function appendStaticAttributes(DOMElement $node, XmlElementAnnotation $annotation): DOMElement
    {
        $staticAttributes = $annotation->attributes;
        foreach ($staticAttributes as $attrName => $attrValue) {
            $node->setAttribute($attrName, $attrValue);
        }

        return $node;
    }

    /**
     * Appends attributes from property annotated with "XmlAttributes"
     *
     * @param DOMElement      $node
     * @param ReflectionClass $reflection
     * @param object          $object
     *
     * @return DOMElement
     */
    private function appendAttributes(DOMElement $node, ReflectionClass $reflection, $object): DOMElement
    {
        foreach ($reflection->getProperties() as $property) {
            /** @var XmlAttributes $attributesAnnotation */
            $attributesAnnotation = $this->reader->getPropertyAnnotation($property, XmlAttributes::class);
            if (!$attributesAnnotation) {
                continue;
            }

            $annotatedProperty = new Property($property, $object);

            foreach ($annotatedProperty->getValue() as $attrName => $attrValue) {
                $node->setAttribute($attrName, $attrValue);
            }
        }

        return $node;
    }

    /**
     * Appends array of elements to $parent.
     *
     * @param            $nodeName
     * @param            $array
     * @param DOMElement $parent
     *
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws StringElementDoesntHaveValue
     * @throws TargetNotMatchException
     */
    private function appendArray($nodeName, $array, DOMElement $parent)
    {
        foreach ($array as $item) {
            if (is_array($item) || $item instanceof ArrayAccess) {
                $this->appendArray($nodeName, $item, $parent);
                continue;
            }

            if (is_object($item)) {
                $this->appendObject($item, $parent, $nodeName);
                continue;
            }

            $this->appendString($nodeName, $item, $parent);
        }
    }

    /**
     * Appends string value to $node.
     *
     * @param string     $nodeName
     * @param string     $nodeValue
     * @param DOMElement $node
     */
    private function appendString(string $nodeName, $nodeValue, DOMElement $node)
    {
        $node->appendChild(
            $this->document->createElement($nodeName, $nodeValue)
        );
    }

    /**
     * Returns document generated from $object.
     *
     * @return DOMDocument
     */
    public function getDocument()
    {
        return $this->document;
    }
}