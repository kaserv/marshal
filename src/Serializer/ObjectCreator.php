<?php

namespace Sk\Marshal\Serializer;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use DOMAttr;
use DOMDocument;
use DOMElement;
use DOMText;
use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use SimpleXMLElement;
use Sk\Marshal\Annotations\XmlAttributes;
use Sk\Marshal\Annotations\XmlElement;
use Sk\Marshal\Annotations\XmlElementAnnotation;
use Sk\Marshal\Annotations\XmlElementEnum;
use Sk\Marshal\Annotations\XmlElementString;
use Sk\Marshal\Annotations\XmlElementsWrapper;
use Sk\Marshal\ClassMap;
use Sk\Marshal\Exceptions\PropertyIsNotPublicException;
use Sk\Marshal\Exceptions\TagIsNotRegisteredException;
use Sk\Marshal\Exceptions\TargetClassIsNotSetException;
use stdClass;
use function Sk\Marshal\studly;

class ObjectCreator
{
    /** @var string */
    private $unknownProperty;

    /** @var string */
    private $staticAttributes;

    /** @var array|null */
    private $dynamicAttributes;

    /** @var ReflectionClass */
    private $reflection;

    /** @var AnnotationReader */
    private $reader;

    /** @var array */
    private $propertiesList;

    /** @var object */
    private $object;

    /** @var DOMElement */
    private $domElement;

    /** @var ClassMap */
    private $classMap;

    /** @var string */
    private $skipKey;

    /**
     * @var XmlElementAnnotation
     */
    private $annotation;

    /**
     * ObjectCreator constructor.
     *
     * @param DOMElement      $domElement
     * @param ReflectionClass $classReflection
     * @param ClassMap        $classMap
     * @param string          $staticAttributes
     * @param string          $unknownProperty
     *
     * @throws AnnotationException
     */
    public function __construct(
        DOMElement $domElement,
        ReflectionClass $classReflection,
        ClassMap $classMap,
        string $staticAttributes,
        string $unknownProperty
    )
    {
        $this->skipKey = md5(rand(0, 100));

        $this->domElement = $domElement;
        $this->reflection = $classReflection;
        $this->classMap = $classMap;
        $this->staticAttributes = $staticAttributes;
        $this->unknownProperty = $unknownProperty;

        $this->init();
    }

    /**
     * Init ObjectCreator.
     *
     * @throws AnnotationException
     */
    private function init()
    {
        $this->reader = new AnnotationReader();
        $this->annotation = $this->reader->getClassAnnotation($this->reflection, XmlElementAnnotation::class);
        $this->object = $this->reflection->newInstanceWithoutConstructor();

        $this->initPropertiesList();
    }

    /**
     * Set $propertiesList property.
     *
     */
    private function initPropertiesList()
    {
        $this->propertiesList = [];

        foreach ($this->reflection->getProperties() as $property) {
            /** @var XmlElementAnnotation $elementAnnotation */
            $elementAnnotation = $this->reader->getPropertyAnnotation($property, XmlElementAnnotation::class);
            if ($elementAnnotation) {
                $annotatedProperty = new Property($property, $this->object, $elementAnnotation);
                $this->propertiesList[$annotatedProperty->getName()] = [
                    'annotation' => $elementAnnotation,
                    'reflection' => $property,
                ];
                continue;
            }

            $attributesAnnotation = $this->reader->getPropertyAnnotation($property, XmlAttributes::class);
            if ($attributesAnnotation) {
                $this->dynamicAttributes = [
                    'property' => $property->getName(),
                    'annotation' => $attributesAnnotation,
                    'reflection' => $property
                ];
                continue;
            }

            $this->propertiesList[$property->getName()] = [
                'annotation' => null,
                'reflection' => $property
            ];
        }
    }

    /**
     * Create object.
     *
     * @return object
     * @throws PropertyIsNotPublicException
     * @throws TargetClassIsNotSetException
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws TagIsNotRegisteredException
     * @throws Exception
     */
    public function create()
    {
        if ($this->annotation instanceof XmlElementString) {
            if (!property_exists($this->object, 'value')) {
                throw new Exception('Objects with XmlElementString annotation must have public property $value');
            }
            $this->object->value = $this->domElement->nodeValue;
        } else {
            foreach ($this->domElement->childNodes as $childNode) {
                if ($childNode instanceof DOMElement) {
                    $this->setProperty($childNode);
                }
            }
        }

        foreach ($this->domElement->attributes as $attribute) {
            $this->setAttribute($attribute);
        }

        return $this->object;
    }

    /**
     * @param DOMElement $node
     *
     * @throws PropertyIsNotPublicException
     * @throws TargetClassIsNotSetException
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws TagIsNotRegisteredException
     * @throws Exception
     */
    private function setProperty(DOMElement $node)
    {
        $annotation = $this->getAnnotation($node);
        $reflection = $this->getReflection($node);

        if (is_null($annotation)) {
            if (!isset($this->object->{$this->unknownProperty})) {
                $this->object->{$this->unknownProperty} = [];
            }

            $doc = new DOMDocument();
            $root = $doc->createElement('root');
            $root->appendChild($doc->importNode($node, true));

            $this->object->{$this->unknownProperty}[] = new SimpleXMLElement($doc->saveXML($root));

            return;
        }

        if ($annotation instanceof XmlElementsWrapper) {
            $this->setWrapper($reflection);

            foreach ($node->childNodes as $childNode) {
                if ($childNode instanceof DOMElement) {
                    $value = $this->getNodeValue($childNode, null, $annotation->skipUnknown);
                    if ($value != $this->skipKey) {
                        $this->object->{$reflection->getName()}[] = $value;
                    }
                }
            }

            return;
        } elseif ($annotation instanceof XmlElement) {
            $value = $this->getNodeValue($node, $annotation->type, $annotation->skipUnknown);
            if ($value == $this->skipKey) {
                return;
            }
        } elseif ($annotation instanceof XmlElementEnum) {
            $map = array_flip($annotation->map);
            $type = $map[$node->nodeName];

            $value = $this->getNodeValue($node, $type, $annotation->skipUnknown);
        } else {
            throw new Exception('Unknown annotation type');
        }

        $this->setPropertyValue($reflection, $value);
    }

    /**
     * Set object property value.
     *
     * @param ReflectionProperty $propertyReflection
     * @param mixed              $propertyValue
     *
     * @throws PropertyIsNotPublicException
     */
    private function setPropertyValue($propertyReflection, $propertyValue)
    {
        $propertyName = $propertyReflection->getName();

        $setter = 'set' . studly($propertyName);
        if (method_exists($this->object, $setter)) {
            $this->object->{$setter}($propertyValue);
            return;
        }

        if (!$propertyReflection->isPublic()) {
            throw new PropertyIsNotPublicException($propertyName);
        }

        if (!empty($this->object->{$propertyName})) {
            if (!is_array($this->object->{$propertyName})) {
                $this->object->{$propertyName} = [$this->object->{$propertyName}];
            }
            $this->object->{$propertyName}[] = $propertyValue;
            return;
        }

        $this->object->{$propertyName} = $propertyValue;
    }

    /**
     * @param DOMAttr $attribute
     *
     * @throws PropertyIsNotPublicException
     * @throws ReflectionException
     */
    private function setAttribute(DOMAttr $attribute)
    {
        if (!empty($this->dynamicAttributes['property'])) {
            $reflection = $this->dynamicAttributes['reflection'];
            $this->setPropertyValue($reflection, $attribute->value);
            return;
        }

        $fakeObject = new stdClass();
        $fakeObject->{$this->staticAttributes} = null;
        $fakeReflection = new ReflectionProperty($fakeObject, $this->staticAttributes);

        /*
         * If class don't have property with name in $this->staticAttributes
         * then it will generate Undefined property notice.
         */
        $this->setPropertyValue($fakeReflection, $attribute->value);

    }

    /**
     * Set wrapper element.
     *
     * @param ReflectionProperty $propertyReflection
     *
     * @throws PropertyIsNotPublicException
     */
    private function setWrapper(ReflectionProperty $propertyReflection)
    {
        $propertyName = $propertyReflection->getName();

        if (!$propertyReflection->isPublic()) {
            throw new PropertyIsNotPublicException($propertyName);
        }

        if (empty($this->object->{$propertyName})) {
            $this->object->{$propertyName} = [];
        }

        if (!is_array($this->object->{$propertyName})) {
            $this->object->{$propertyName} = [$this->object->{$propertyName}];
        }
    }

    /**
     * Returns value of node.
     *
     * @param DOMElement $node
     * @param null       $targetClass
     * @param bool       $skipIfUnknown
     *
     * @return mixed
     * @throws AnnotationException
     * @throws PropertyIsNotPublicException
     * @throws ReflectionException
     * @throws TagIsNotRegisteredException
     * @throws TargetClassIsNotSetException
     */
    private function getNodeValue(DOMElement $node, $targetClass = null, $skipIfUnknown = false)
    {
        if ($node->childNodes->count() == 0) {
            return null;
        }

        if (is_null($targetClass)) {
            if ($node->childNodes->count() == 1 && $node->firstChild instanceof DOMText) {
                return $node->nodeValue;
            }

            try {
                $targetClass = $this->classMap->getClass($node->nodeName);
            } catch (TagIsNotRegisteredException $exception) {
                if ($skipIfUnknown) {
                    return $this->skipKey;
                }

                throw $exception;
            }
        }

        $reflection = new ReflectionClass($targetClass);

        return (new self(
            $node,
            $reflection,
            $this->classMap,
            $this->staticAttributes,
            $this->unknownProperty)
        )->create();
    }

    /**
     * @param DOMElement $node
     *
     * @return XmlElementAnnotation|null
     */
    private function getAnnotation(DOMElement $node)
    {
        if (isset($this->propertiesList[$node->nodeName])) {
            return $this->propertiesList[$node->nodeName]['annotation'];
        }

        foreach ($this->propertiesList as $propertyName => $config) {
            $propAnnotation = $config['annotation'];
            if ($propAnnotation instanceof XmlElementEnum) {
                if (in_array($node->nodeName, $propAnnotation->map)) {
                    return $propAnnotation;
                }
            }
        }

        return null;
    }

    /**
     * @param DOMElement $node
     *
     * @return ReflectionProperty|null
     */
    private function getReflection(DOMElement $node)
    {
        if (isset($this->propertiesList[$node->nodeName])) {
            return $this->propertiesList[$node->nodeName]['reflection'];
        }

        foreach ($this->propertiesList as $propertyName => $config) {
            $propAnnotation = $config['annotation'];
            if ($propAnnotation instanceof XmlElementEnum) {
                if (in_array($node->nodeName, $propAnnotation->map)) {
                    return $this->propertiesList[$propertyName]['reflection'];
                }
            }
        }

        return null;
    }
}