<?php

namespace Sk\Marshal\Serializer;

use ReflectionProperty;
use Sk\Marshal\Annotations\XmlElementAnnotation;
use Sk\Marshal\Annotations\XmlElementEnum;
use function Sk\Marshal\studly;

class Property
{
    /** @var ReflectionProperty */
    private $reflectionProperty;

    /** @var object */
    private $object;

    /** @var XmlElementAnnotation|null */
    private $annotation;

    /**
     * Property constructor.
     *
     * @param ReflectionProperty   $reflectionProperty
     * @param object               $object
     * @param XmlElementAnnotation $annotation
     */
    public function __construct(ReflectionProperty $reflectionProperty, $object, XmlElementAnnotation $annotation = null)
    {
        $this->reflectionProperty = $reflectionProperty;
        $this->object = $object;
        $this->annotation = $annotation;
    }

    /**
     * Get name of property.
     *
     * @return string
     */
    public function getName(): string
    {
        if (!$this->annotation) {
            return $this->reflectionProperty->getName();
        }

        $name = '';

        $namespace = $this->annotation->namespace;
        if (!empty($namespace)) {
            $name = $namespace . ':';
        }

        if ($this->annotation instanceof XmlElementEnum) {
            $map = $this->annotation->map;
            $value = $this->getValue();
            if (is_null($value)) {
                $name .= $this->annotation->name ?: $this->reflectionProperty->getName();
            } else {
                $type = get_class($value);
                if (isset($map[$type])) {
                    $name .= $map[$type];
                } else {
                    $name .= $this->annotation->name ?: $this->reflectionProperty->getName();
                }
            }
        } else {
            $name .= $this->annotation->name ?: $this->reflectionProperty->getName();
        }

        return $name;
    }

    /**
     * Return value of property.
     *
     * @return mixed
     */
    public function getValue()
    {
        $base = studly($this->reflectionProperty->getName());

        $is = 'is' . $base;
        if (method_exists($this->object, $is)) {
            return $this->object->{$is}();
        }

        $get = 'get' . $base;
        if (method_exists($this->object, $get)) {
            return $this->object->{$get}();
        }

        return $this->reflectionProperty->getValue($this->object);
    }
}