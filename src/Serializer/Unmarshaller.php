<?php

namespace Sk\Marshal\Serializer;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use DOMDocument;
use ReflectionClass;
use ReflectionException;
use Sk\Marshal\Annotations\XmlDocument;
use Sk\Marshal\ClassMap;
use Sk\Marshal\Exceptions\PropertyIsNotPublicException;
use Sk\Marshal\Exceptions\TagIsNotRegisteredException;
use Sk\Marshal\Exceptions\TagNotMatchException;
use Sk\Marshal\Exceptions\TargetClassIsNotSetException;

class Unmarshaller
{
    /** @var ClassMap */
    private $classMap;

    /** @var AnnotationReader */
    private $reader;

    /** @var string */
    private $staticAttributes;

    /** @var string */
    private $unknownProperty;

    /** @var DOMDocument */
    private $dom;

    /** @var ReflectionClass */
    private $reflection;

    /** @var XmlDocument */
    private $documentAnnotation;

    /**
     * Unmarshaller constructor.
     *
     * @param string   $xml              Input xml
     * @param ClassMap $classMap         ClassMap object
     * @param string   $staticAttributes Property name for attributes
     *                                   if object has no XmlAttributes annotated property
     * @param string   $unknownProperty  Property name of result object
     *                                   for storing values of unknown tags
     *
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws TagIsNotRegisteredException
     * @throws TagNotMatchException
     */
    public function __construct(string $xml, ClassMap $classMap, string $staticAttributes = '__staticAttributes', string $unknownProperty = '__unknownProperties')
    {
        $this->classMap = $classMap;
        $this->staticAttributes = $staticAttributes;
        $this->unknownProperty = $unknownProperty;

        $this->init($xml);
    }

    /**
     * Build object.
     *
     * @return object
     * @throws AnnotationException
     * @throws PropertyIsNotPublicException
     * @throws ReflectionException
     * @throws TagIsNotRegisteredException
     * @throws TargetClassIsNotSetException
     */
    public function getObject()
    {
        return (new ObjectCreator(
            $this->dom->documentElement,
            $this->reflection,
            $this->classMap,
            $this->staticAttributes,
            $this->unknownProperty
        ))->create();
    }

    /**
     * Check tag name.
     *
     * @throws TagNotMatchException
     */
    private function checkTagName()
    {
        $expectedName = '';
        if ($this->documentAnnotation->namespace) {
            $expectedName .= $this->documentAnnotation->namespace . ':';
        }
        $expectedName .= $this->documentAnnotation->name ?: $this->reflection->getShortName();

        if ($expectedName != $this->dom->documentElement->nodeName) {
            throw new TagNotMatchException($this->dom->documentElement->nodeName, $expectedName);
        }
    }

    /**
     * @param $xml
     *
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws TagNotMatchException
     * @throws TagIsNotRegisteredException
     */
    private function init($xml)
    {
        $this->reader = new AnnotationReader();

        $this->dom = new DOMDocument();
        $this->dom->loadXML($xml);

        $class = $this->classMap->getClass($this->dom->documentElement->nodeName);
        $this->reflection = new ReflectionClass($class);
        $this->documentAnnotation = $this->reader->getClassAnnotation($this->reflection, XmlDocument::class);

        $this->checkTagName();
    }
}