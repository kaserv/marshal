<?php

namespace Sk\Marshal;

/**
 * Returns $str in studly case.
 *
 * @param $str
 *
 * @return string
 */
function studly(string $str)
{
    return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $str)));
}